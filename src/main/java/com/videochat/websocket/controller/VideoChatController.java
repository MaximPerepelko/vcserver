package com.videochat.websocket.controller;

import com.videochat.payload.IceCandidate;
import com.videochat.payload.SdpAnswer;
import com.videochat.payload.SdpOffer;
import com.videochat.service.VideoChatService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.simp.SimpMessageHeaderAccessor;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.messaging.simp.annotation.SendToUser;
import org.springframework.stereotype.Controller;

@Controller
public class VideoChatController {

    @Autowired
    private VideoChatService service;

    @MessageMapping("/offer")
//    @SendToUser(destinations = "/queue/offer", broadcast = false)
//    @SendTo("/topic/greetings")
    public void offer(SdpOffer sdpOffer, SimpMessageHeaderAccessor headerAccessor) {
        String sessionId = headerAccessor.getSessionId();
        service.resolveOffer(sessionId, sdpOffer);
    }

    @MessageMapping("/answer")
    public void answer(SdpAnswer sdpAnswer, SimpMessageHeaderAccessor headerAccessor) {
        String sessionId = headerAccessor.getSessionId();
        service.resolveAnswer(sessionId, sdpAnswer);
    }

    @MessageMapping("/icecandidate")
    public void icecandidate(IceCandidate icecandidate, SimpMessageHeaderAccessor headerAccessor) {
        String sessionId = headerAccessor.getSessionId();
        service.resolveIceCandidate(sessionId, icecandidate);
    }


//    @MessageExceptionHandler
//    @SendToUser("/queue/errors")
//    public String handleException(Throwable exception) {
//        return exception.getMessage();
//    }

}
