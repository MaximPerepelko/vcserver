package com.videochat.websocket;

import org.springframework.context.ApplicationListener;
import org.springframework.messaging.simp.stomp.StompHeaderAccessor;
import org.springframework.stereotype.Component;
import org.springframework.web.socket.messaging.SessionConnectedEvent;

@Component
public class StompConnectedEvent implements ApplicationListener<SessionConnectedEvent> {

    public void onApplicationEvent(SessionConnectedEvent connectedEvent) {
        StompHeaderAccessor stompHeaderAccessor = StompHeaderAccessor.wrap(connectedEvent.getMessage());
//        Principal user = event.getUser();
//        String sessionId = stompHeaderAccessor.getSessionId();
    }
}
