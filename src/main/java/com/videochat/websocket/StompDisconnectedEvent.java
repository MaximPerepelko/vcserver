package com.videochat.websocket;

import com.videochat.service.VideoChatService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.messaging.simp.stomp.StompHeaderAccessor;
import org.springframework.stereotype.Component;
import org.springframework.web.socket.messaging.SessionDisconnectEvent;

@Component
public class StompDisconnectedEvent implements ApplicationListener<SessionDisconnectEvent> {

    @Autowired
    private VideoChatService chatService;

    public void onApplicationEvent(SessionDisconnectEvent disconnectEvent) {
        StompHeaderAccessor stompHeaderAccessor = StompHeaderAccessor.wrap(disconnectEvent.getMessage());
//        Principal user = event.getUser();
        String sessionId = stompHeaderAccessor.getSessionId();
        chatService.disconnectUsers(sessionId);
    }
}
