package com.videochat;

import com.googlecode.cqengine.codegen.AttributeSourceGenerator;
import com.videochat.chat.UserSession;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.messaging.simp.config.ChannelRegistration;
import org.springframework.messaging.simp.config.MessageBrokerRegistry;
import org.springframework.web.socket.config.annotation.*;

@SpringBootApplication
//@EnableWebSocket

//public class Application implements WebSocketConfigurer {
public class Application {

	public static void main(String[] args) {
//        System.out.println(AttributeSourceGenerator.generateAttributesForPastingIntoTargetClass(UserSession.class));
		SpringApplication.run(Application.class, args);
	}

    //	private SimpMessagingTemplate simpMessagingTemplate;
//
//	@Bean
//	public CallHandler callHandler() {
//		return new CallHandler();
//	}
//
//	@Override
//	public void registerWebSocketHandlers(WebSocketHandlerRegistry registry) {
//		registry.addHandler(callHandler(), "/call");
//	}
//	public TradeResult executeTrade(Trade trade, Principal principal) {
//		simpMessagingTemplate.convertAndSendToUser();
//		principal.getName()
//		return tradeResult;
//	}
}
