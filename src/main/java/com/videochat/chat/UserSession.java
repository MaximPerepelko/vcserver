package com.videochat.chat;

import com.googlecode.cqengine.attribute.Attribute;
import com.googlecode.cqengine.attribute.SimpleAttribute;
import com.googlecode.cqengine.query.option.QueryOptions;
import com.videochat.domain.Country;
import com.videochat.domain.Gender;
import com.videochat.domain.Preference;
import com.videochat.payload.SdpOffer;

public class UserSession {

    private String sessionId;
    private SdpOffer sdpOffer;
    private UserSession partnerSession;

    private Gender gender = Gender.ANY;
    private Country country = Country.ANY;
    private Preference preference = new Preference();

    public UserSession(String sessionId, SdpOffer sdpOffer) {
        this.sessionId = sessionId;
        this.sdpOffer = sdpOffer;
    }

    public String getSessionId() {
        return sessionId;
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

    public SdpOffer getSdpOffer() {
        return sdpOffer;
    }

    public void setSdpOffer(SdpOffer sdpOffer) {
        this.sdpOffer = sdpOffer;
    }

    public UserSession getPartnerSession() {
        return partnerSession;
    }

    public void setPartnerSession(UserSession partnerSession) {
        this.partnerSession = partnerSession;
    }

    public Gender getGender() {
        return gender;
    }

    public void setGender(Gender gender) {
        this.gender = gender;
    }

    public Country getCountry() {
        return country;
    }

    public void setCountry(Country country) {
        this.country = country;
    }

    public Preference getPreference() {
        return preference;
    }

    public void setPreference(Preference preference) {
        this.preference = preference;
    }

    /**
     * CQEngine attribute for accessing field {@code UserSession.gender}.
     */
    // Note: For best performance:
    // - if this field cannot be null, replace this SimpleNullableAttribute with a SimpleAttribute
    public static final Attribute<UserSession, Gender> GENDER = new SimpleAttribute<UserSession, Gender>("GENDER") {
        public Gender getValue(UserSession usersession, QueryOptions queryOptions) { return usersession.gender; }
    };

    /**
     * CQEngine attribute for accessing field {@code UserSession.country}.
     */
    // Note: For best performance:
    // - if this field cannot be null, replace this SimpleNullableAttribute with a SimpleAttribute
    public static final Attribute<UserSession, Country> COUNTRY = new SimpleAttribute<UserSession, Country>("COUNTRY") {
        public Country getValue(UserSession usersession, QueryOptions queryOptions) { return usersession.country; }
    };

    /**
     * CQEngine attribute for accessing field {@code UserSession.sessionId}.
     */
    // Note: For best performance:
    // - if this field cannot be null, replace this SimpleNullableAttribute with a SimpleAttribute
    public static final Attribute<UserSession, String> SESSION_ID = new SimpleAttribute<UserSession, String>("SESSION_ID") {
        public String getValue(UserSession usersession, QueryOptions queryOptions) { return usersession.sessionId; }
    };

    /**
     * CQEngine attribute for accessing field {@code UserSession.preference.gender}.
     */
    // Note: For best performance:
    // - if this field cannot be null, replace this SimpleNullableAttribute with a SimpleAttribute
    public static final Attribute<UserSession, Gender> PREFERENCE_GENDER = new SimpleAttribute<UserSession, Gender>("PREFERENCE_GENDER") {
        public Gender getValue(UserSession userSession, QueryOptions queryOptions) { return userSession.preference.getGender(); }
    };

    /**
     * CQEngine attribute for accessing field {@code UserSession.preference.gender}.
     */
    // Note: For best performance:
    // - if this field cannot be null, replace this SimpleNullableAttribute with a SimpleAttribute
    public static final Attribute<UserSession, Country> PREFERENCE_COUNTRY = new SimpleAttribute<UserSession, Country>("PREFERENCE_COUNTRY") {
        public Country getValue(UserSession userSession, QueryOptions queryOptions) { return userSession.preference.getCountry(); }
    };

}