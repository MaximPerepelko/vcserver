package com.videochat.chat;

import com.googlecode.cqengine.IndexedCollection;
import com.googlecode.cqengine.ObjectLockingIndexedCollection;
import com.googlecode.cqengine.index.hash.HashIndex;
import com.googlecode.cqengine.index.unique.UniqueIndex;
import com.googlecode.cqengine.resultset.ResultSet;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import static com.google.common.collect.Lists.newArrayList;
import static com.googlecode.cqengine.query.QueryFactory.equal;
import static com.videochat.chat.Matcher.exactMatch;
import static com.videochat.chat.Matcher.exactOrAnyMatch;
import static com.videochat.chat.UserSession.*;
import static java.util.Objects.nonNull;

@Component
public class VideoChatContext {

    private Map<String, UserSession> usersConnected = new ConcurrentHashMap<>();
    private IndexedCollection<UserSession> waitingUsers = new ObjectLockingIndexedCollection<>();

    @PostConstruct
    public void addIndexes() {
        waitingUsers.addIndex(HashIndex.onAttribute(PREFERENCE_GENDER));
        waitingUsers.addIndex(HashIndex.onAttribute(PREFERENCE_COUNTRY));
        waitingUsers.addIndex(HashIndex.onAttribute(GENDER));
        waitingUsers.addIndex(HashIndex.onAttribute(COUNTRY));
        waitingUsers.addIndex(UniqueIndex.onAttribute(SESSION_ID));
    }

    public UserSession clearSessionContext(String sessionId) {
        removeFromQueue(sessionId);
        return usersConnected.remove(sessionId);
    }

    public UserSession getPartnerSession(String sessionId) {
        UserSession userSession = usersConnected.get(sessionId);
        if (nonNull(userSession)) {
            return userSession.getPartnerSession();
        }
        return null;
    }

    public void addConnected(UserSession userSession, UserSession matchedUserSession) {
        usersConnected.put(userSession.getSessionId(), userSession);
        usersConnected.put(matchedUserSession.getSessionId(), matchedUserSession);
    }

    public void addToQueue(UserSession userSession) {
        waitingUsers.add(userSession);
    }

    public void removeFromQueue(String sessionId) {
        ResultSet<UserSession> result = waitingUsers.retrieve(equal(SESSION_ID, sessionId));
        if (!result.isEmpty()) {
            waitingUsers.removeAll(newArrayList(result.iterator()));
        }
    }

    public ResultSet<UserSession> pollExactMatch(UserSession userSession) {
        return waitingUsers.retrieve(exactMatch(userSession));
    }

    public ResultSet<UserSession> pollExactOrAnyMatch(UserSession userSession) {
        return waitingUsers.retrieve(exactOrAnyMatch(userSession));
    }

}
