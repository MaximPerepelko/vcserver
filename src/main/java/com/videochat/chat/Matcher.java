package com.videochat.chat;

import com.googlecode.cqengine.query.Query;
import com.videochat.domain.Country;
import com.videochat.domain.Gender;

import java.util.Set;

import static com.google.common.collect.Lists.newArrayList;
import static com.googlecode.cqengine.query.QueryFactory.*;
import static com.googlecode.cqengine.query.QueryFactory.equal;
import static com.googlecode.cqengine.query.QueryFactory.in;
import static com.videochat.chat.UserSession.*;
import static com.videochat.chat.UserSession.COUNTRY;
import static org.hibernate.validator.internal.util.CollectionHelper.asSet;

public class Matcher {

    public static Query exactOrAnyMatch(UserSession userSession) {
        Set<Gender> genderPrefs = asSet(userSession.getPreference().getGender());
        if (userSession.getPreference().getGender() == Gender.ANY) {
            genderPrefs.addAll(newArrayList(Gender.values()));
        }

        Set<Country> countryPrefs = asSet(userSession.getPreference().getCountry());
        if (userSession.getPreference().getCountry() == Country.ANY) {
            countryPrefs.addAll(newArrayList(Country.values()));
        }

        return and(
                or(equal(PREFERENCE_GENDER, userSession.getGender()),
                        equal(PREFERENCE_GENDER, Gender.ANY)),
                or(equal(PREFERENCE_COUNTRY, userSession.getCountry()),
                        equal(PREFERENCE_COUNTRY, Country.ANY)),
                in(GENDER, genderPrefs),
                in(COUNTRY, countryPrefs));
    }

    public static Query exactMatch(UserSession userSession) {
        Set<Gender> genderPrefs = asSet(userSession.getPreference().getGender());
        if (userSession.getPreference().getGender() == Gender.ANY) {
            genderPrefs.addAll(newArrayList(Gender.values()));
        }

        Set<Country> countryPrefs = asSet(userSession.getPreference().getCountry());
        if (userSession.getPreference().getCountry() == Country.ANY) {
            countryPrefs.addAll(newArrayList(Country.values()));
        }

        return and(
                equal(PREFERENCE_GENDER, userSession.getGender()),
                equal(PREFERENCE_COUNTRY, userSession.getCountry()),
                in(GENDER, genderPrefs),
                in(COUNTRY, countryPrefs));
    }

}
