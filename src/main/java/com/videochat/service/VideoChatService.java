package com.videochat.service;

import com.googlecode.cqengine.resultset.ResultSet;
import com.videochat.chat.UserSession;
import com.videochat.chat.VideoChatContext;
import com.videochat.payload.IceCandidate;
import com.videochat.payload.SdpAnswer;
import com.videochat.payload.SdpOffer;
import com.videochat.payload.TerminateCall;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import static java.util.Objects.nonNull;

@Component
public class VideoChatService {
                                            //TODO FIXME override hash and equals !!!!!!!!!!!!!!!
    @Autowired
    private StompService stompService;

    @Autowired
    private VideoChatContext chatContext;

    public void resolveOffer(String sessionId, SdpOffer sdpOffer) {
        UserSession userSession = disconnectUsers(sessionId);
        if (nonNull(userSession)) {
            userSession.setSdpOffer(sdpOffer);
            userSession.setPartnerSession(null);
        } else {
            userSession = new UserSession(sessionId, sdpOffer);
        }

        ResultSet<UserSession> match = chatContext.pollExactMatch(userSession);
        if (match.isEmpty()) {
            match = chatContext.pollExactOrAnyMatch(userSession);  // TODO FIXME fix by user profile type
        }
        if (match.isEmpty()) {
           chatContext.addToQueue(userSession);
        } else {
            UserSession matchedUserSession = match.iterator().next();
            chatContext.removeFromQueue(matchedUserSession.getSessionId());
            connectUsers(userSession, matchedUserSession);
        }
        match.close();
    }

    public UserSession disconnectUsers(String sessionId) {
        UserSession userSession = chatContext.clearSessionContext(sessionId);
        if (nonNull(userSession)) {
            UserSession partnerSession = userSession.getPartnerSession();

            if (nonNull(partnerSession)) {
                stompService.sendToUser(partnerSession.getSessionId(), new TerminateCall());
                chatContext.clearSessionContext(partnerSession.getSessionId());
            }
        }
        return userSession;
    }

    private void connectUsers(UserSession userSession, UserSession matchedUserSession) {
        userSession.setPartnerSession(matchedUserSession);
        matchedUserSession.setPartnerSession(userSession);
        stompService.sendToUser(matchedUserSession.getSessionId(), userSession.getSdpOffer());
        chatContext.addConnected(userSession, matchedUserSession);
    }

    public void resolveAnswer(String sessionId, SdpAnswer sdpAnswer) {
        UserSession partnerSession = chatContext.getPartnerSession(sessionId);
        if (nonNull(partnerSession)) {
            stompService.sendToUser(partnerSession.getSessionId(), sdpAnswer);
        }
    }

    public void resolveIceCandidate(String sessionId, IceCandidate iceCandidate) {
        UserSession partnerSession = chatContext.getPartnerSession(sessionId);
        if (nonNull(partnerSession)) {
            stompService.sendToUser(partnerSession.getSessionId(), iceCandidate);
        }
    }

}
