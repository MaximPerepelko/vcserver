package com.videochat.domain;

public class Preference {

    private Gender gender = Gender.ANY;
    private Country country = Country.ANY;

    public Gender getGender() {
        return gender;
    }

    public void setGender(Gender gender) {
        this.gender = gender;
    }

    public Country getCountry() {
        return country;
    }

    public void setCountry(Country country) {
        this.country = country;
    }

}
