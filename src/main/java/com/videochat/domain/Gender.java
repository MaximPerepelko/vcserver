package com.videochat.domain;

public enum Gender {

    MALE,
    FEMALE,
    ANY

}
