package com.videochat.payload;

public interface Payload {

    String getDestination();

}
