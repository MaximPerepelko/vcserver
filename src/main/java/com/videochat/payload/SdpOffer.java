package com.videochat.payload;

public class SdpOffer implements Payload {

    private static final String DESTINATION = "/queue/offer";

    private String sdp;
    private String type;

    public String getSdp() {
        return sdp;
    }

    public void setSdp(String sdp) {
        this.sdp = sdp;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @Override
    public String getDestination() {
        return DESTINATION;
    }

}
