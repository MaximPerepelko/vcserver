package com.videochat.payload;

public class TerminateCall implements Payload {

    private static final String DESTINATION = "/queue/terminatecall";

    @Override
    public String getDestination() {
        return DESTINATION;
    }

}
